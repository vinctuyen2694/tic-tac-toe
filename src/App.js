import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      board: Array(9).fill(null),
      player: "X",
      winner: null,
      stepNum: 0,
    }
  }

  checkWinner() {
    let winLines =
      [

        ["0", "1", "2"],
        ["3", "4", "5"],
        ["6", "7", "8"],
        ["0", "3", "6"],
        ["1", "4", "7"],
        ["2", "5", "8"],
        ["0", "4", "8"],
        ["2", "4", "6"],

      ]
    this.checkMatch(winLines)
  }

  checkMatch(winLines) {

    for (let index = 0; index < winLines.length; index++) {
      this.setState({ stepNum: this.state.stepNum + 1 })
      if (this.state.stepNum === 8) {
        alert('Hòa, Game Over');
        break;
      }
      console.log(this.state.stepNum)
      const [a, b, c] = winLines[index];
      let board = this.state.board
      if (board[a] && board[a] === board[b] && board[a] === board[c]) {
        alert(this.state.player + ' Win, Game Over');
        this.setState({
          winner: this.state.player,
        })
      }

    }
  }

  handleClick(index) {
    let newBoard = this.state.board
    if (this.state.winner === null && this.state.stepNum !== 9) {
      if (this.state.board[index] === null && !this.state.winner) {
        newBoard[index] = this.state.player
        this.setState({
          board: newBoard,
          player: this.state.player === "X" ? "O" : "X",
        })
        this.checkWinner()
      }
    }
  }


  render() {

    const Box = this.state.board.map(

      (box, index) =>

        <div className="box"

          key={index}

          onClick={() => this.handleClick(index)}>

          {box}

        </div>)
    return (
      <div className="container">
        <div className="board">
          {Box}

        </div>
      </div>
    );
  }
}

export default App;
